FROM node:18 as build


WORKDIR /app


COPY package*.json ./

RUN npm install -g npm@10.2.5


RUN npm install

COPY . .


RUN npm run build


FROM nginx:mainline-alpine3.18


COPY --from=build /app/build /usr/share/nginx/html

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]



 